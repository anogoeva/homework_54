import React from 'react';

const Tries = ({ count }) => {
    return (
        <div>
            <h3>Tries: {count}</h3>
        </div>
    );
};
export default Tries;