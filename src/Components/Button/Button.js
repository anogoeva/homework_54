import React from 'react';

const Button = ({resetCount}) => {
    return (
        <div>
            <button type="reset" onClick={resetCount}>Reset</button>
        </div>
    );
};

export default Button;