import React, { useState } from 'react';
import './Square.css';

const Square = props => {
    const [isClicked, setClicked] = useState(false)

    const handleClick = () => {
        setClicked(true)
        props.toShow()
    }

    return isClicked ? 
            <div key={props.key} className="square" onClick={handleClick}>
                {props.isHasRing ? 'O' : 'Ring isn`t here'}
            </div> 
        : <div key={props.key} className="square" onClick={handleClick}>Click</div>
};

export default Square;