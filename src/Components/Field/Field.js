import './Field.css';
import Square from "../Square/Square";

const randomNumber = Math.floor(Math.random() * (36)); 
const Field = ({ count, setCount }) => {
    const fields = []
    console.log(randomNumber)

    const find = key => {
        const keys = fields.map(item => Number(item.key))
        const kyeIndex = keys.includes(key);
        console.log(kyeIndex);
    };

    const handleShowSquare = (i) => {
        find(i)
        setCount(count+=1)
    }

    for (let i = 0; i < 36; i++) {
        fields.push(
            <Square
                key={i}
                isHasRing={i === randomNumber}
                toShow={() => handleShowSquare(i)}
            />
        )
    }

    return (
        <div className="container">
            {fields.map(item => item)}
        </div>
    );
};

export default Field;