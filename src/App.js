import { useState } from 'react'
import './App.css';
import Field from "./Components/Field/Field";
import Tries from "./Components/Tries/Tries";
import Button from "./Components/Button/Button";

function App() {
    const [count, setCount] = useState(0)
    return (
    <div className="App">
        <Field count={count} setCount={setCount} />
        <Tries count={count} />
        <Button resetCount={() => setCount(0)} />
    </div>
  );
}
export default App;
